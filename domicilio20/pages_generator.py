import os

# Gruppi di tablet ID
operatori = ["101", "102"]
centro_ASC = ("asc", ["8", "9", "10", "11", "12", "13", "14", "15"])
centro_ASP = ("asp", ["1", "2", "3", "4", "5", "6", "7"])
centri = [centro_ASC, centro_ASP]

# Apri HTML template
template = open("pages/template.html", "r")
lines = template.readlines()

# Scorro le pagine diverse dal template
for page in os.listdir("pages"): 
    ID = page.split(".")[0]   
    if (ID != "template") and (ID not in operatori):
        print(ID)
        html = open("pages/" + page, "w")        
        # Gli riscrivo sopra le stesse righe del template ma...
        for line in lines:
            # sostituendo i TABLET ID
            if ("[TABLET_ID]" in line):
                line = line.replace("[TABLET_ID]", ID)
            # e sostituendo il nome del centro
            if ("[NOME_CENTRO]" in line):
                for centro in centri:
                    if ID in centro[1]:           
                        line = line.replace("[NOME_CENTRO]", centro[0])
            html.write(line)
        html.close()

template.close()
print("END")
