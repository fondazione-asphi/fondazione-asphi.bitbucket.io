import os
from shutil import copyfile


files = os.listdir()

def replace_loader(game):
    print(game)
    copyfile("UnityLoader.js", game + "/stable/Build/UnityLoader.js")

for f in files:
    if os.path.isdir(f):
        replace_loader(f)